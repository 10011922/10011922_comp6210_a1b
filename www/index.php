<?php include_once('functions/functions.php'); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

        <title>Home</title>
    </head>
    <body>
    <div class="container-fluid"> <!-- not indenting this -->
        <!-- TOP BAR -->
        <div class="row">
            <div class="col-6 col-sm-4">
                <img src="../src/img/tst.jpeg" class="img-fluid" alt="Responsive image">
            </div>
            <div class="col-6 col-sm-8">
                <!-- TOP ROW - LOGIN & SIGNUP -->
                <div class="row justify-content-end" style="height:50%">
                    
                    <div class="col-12 col-sm-2">
                        <nav class="navbar navbar-expand navbar-light nav-fill w-100">
                            <a class="navbar-brand" href="#"></a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                                <div class="navbar-nav  nav-fill w-100">
                                    <!-- Why the shit do these just fly off the page/browser!? -->
                                    <a class="nav-item nav-link" href="#">LOG IN</a>
                                    <a class="nav-item nav-link" href="#">SIGN UP</a>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
                <!-- BOTTOM ROW - NAV ITEMS -->
                <div class="row" style="height:50%">
                    <nav class="navbar navbar-expand-sm navbar-light  nav-fill w-100">
                        <a class="navbar-brand" href="#"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav  nav-fill w-100">
                                <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
                                <a class="nav-item nav-link" href="game.php">Games</a>
                                <a class="nav-item nav-link" href="#">Reviews</a>
                                <a class="nav-item nav-link" href="#">Plays</a>
                                <a class="nav-item nav-link" href="#">Profile</a>
                                <a class="nav-item nav-link" href="#">Leaderboard</a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <!-- CONTENT HERE -->
        <div class="row">
            <div class="col-1 col-md-3"></div>

            <div class="col-10 col-md-6 bg-light">
                <!-- TITLE -->
                <div class="row">
                    <div class="col-xs-12 ceter-block text-center"> <h1 class="text-center">Spent a few hours trying to center these...</h1> </div>
                </div>
                <!-- CONTENT -->
                <div class="row">
                    <img src="../src/img/pic1.jpg" class="rounded float-left" style="width:33%; height:auto" alt="picture">
                    <p>WHY THE FUCK IS THAT IMAGE ROTATED!?, consectetur adipiscing elit. Maecenas dapibus elit metus, ac finibus sapien blandit nec. Sed cursus id neque et viverra. Curabitur vulputate, ante et lobortis aliquet, nibh nisl convallis justo, at dictum tortor augue at magna. Maecenas vestibulum ligula sem. Phasellus sodales magna non tortor malesuada, sed rhoncus tortor varius. Nulla facilisi. Nulla vel ex commodo, consequat est nec, efficitur elit. Donec faucibus nec mi sit amet maximus. Vivamus et luctus turpis. Suspendisse potenti. Vivamus quis aliquam sapien. Donec scelerisque turpis a scelerisque scelerisque. Sed commodo lorem nulla, eget semper odio luctus in. Live Reload is not possible without body or head tag.</p>
                </div>
            
                <!-- TITLE -->
                <div class="row">
                    <div class="col-xs-12 ceter-block text-center"> <h1 class="text-center">Title</h1> </div>
                </div>
                <!-- CONTENT -->
                <div class="row">
                    <img src="../src/img/pic1.jpg" class="rounded float-left" style="width:33%; height:auto" alt="picture">
                    <p>WHY THE FUCK IS THAT IMAGE ROTATED!?, consectetur adipiscing elit. Maecenas dapibus elit metus, ac finibus sapien blandit nec. Sed cursus id neque et viverra. Curabitur vulputate, ante et lobortis aliquet, nibh nisl convallis justo, at dictum tortor augue at magna. Maecenas vestibulum ligula sem. Phasellus sodales magna non tortor malesuada, sed rhoncus tortor varius. Nulla facilisi. Nulla vel ex commodo, consequat est nec, efficitur elit. Donec faucibus nec mi sit amet maximus. Vivamus et luctus turpis. Suspendisse potenti. Vivamus quis aliquam sapien. Donec scelerisque turpis a scelerisque scelerisque. Sed commodo lorem nulla, eget semper odio luctus in. Live Reload is not possible without body or head tag.</p>
                </div>
            </div>

            <div class="col-1 col-md-3"></div>
        </div>

        <!-- Almost forgot... -->
        <nav class="navbar navbar-expand navbar-light nav-fill w-100">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav nav-fill w-100">
                    <a class="nav-item nav-link" href="#">About Us</a>
                    <a class="nav-item nav-link" href="#">Feedback</a>
                    <a class="nav-item nav-link" href="#">Contact</a>
                </div>
            </div>
        </nav>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    </div>


    </body>
</html>