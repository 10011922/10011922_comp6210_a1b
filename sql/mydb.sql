/****      DATABASE CREATION                                    *****
- Running this query will create the tables & enter test data
*****                                                           ****/

-- Create Database
CREATE DATABASE IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;

-- Use Database
USE `mydb`;

-- Table structure for table `t_users`
DROP TABLE IF EXISTS `t_users`;
CREATE TABLE `t_users` (
    `ID` int(11) NOT NULL,
    `USERNAME` varchar(20) NOT NULL,
    `PASSWORD` varchar(25) NOT NULL,
    `AVATAR_URL` varchar(255) NOT NULL,
    `CAPTION` varchar(255) NOT NULL,
    PRIMARY KEY ( `ID` )
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Table structure for table `t_scores`
DROP TABLE IF EXISTS `t_scores`;
CREATE TABLE `t_scores` (
    `ID` int(11) NOT NULL,
    `USR_ID` int(11) NOT NULL,
    `DATE` DATE NOT NULL,
    `CLICKS` BIGINT NOT NULL,
    PRIMARY KEY ( `ID` )
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Enter 't_users' test data
INSERT INTO `t_users` (`ID`, `USERNAME`, `PASSWORD`, `AVATAR_URL`, `CAPTION`) VALUES
(1, 'JosIsAwesome', 'cat', 'LINKTODEFAULTIMAGE', 'I am the best'),
(2, 'TheMurlock', 'mrglmrgl', 'LINKTODEFAULTIMAGE', 'MRGLMGRLMGRLMGRL'),
(3, 'TESTUSER', 'pw', 'LINKTODEFAULTIMAGE', 'I am here to test'),
(4, 'ClickFan2005', 'password', 'LINKTODEFAULTIMAGE', 'FOLLOW ME ON TWITTER LOL'),
(5, 'XxXSTRAIGHTEDGEXxX', 'throwdown', 'LINKTODEFAULTIMAGE', 'This is so lame'),
(6, '8008135', 'lol', 'LINKTODEFAULTIMAGE', 'Enter your caption here');

-- Enter 't_scores' test data
INSERT INTO `t_scores` (`ID`, `USR_ID`, `DATE`, `CLICKS`) VALUES
(1, 1, '2008-7-04', 117253),
(2, 1, '2018-4-02', 112313),
(3, 2, '2018-04-03', 5),
(4, 4, '2018-04-04', 112312),
(5, 1, '2018-04-05', 252525),
(6, 5, '2018-04-06', 161616),
(7, 2, '2018-04-07', 15),
(8, 4, '2018-04-08', 1423),
(9, 1, '2018-04-09', 252525),
(10, 1, '2018-04-01', 252525),
(11, 4, '2018-04-02', 1343),
(12, 4, '2018-04-03', 1216),
(13, 4, '2018-04-04', 1236),
(14, 1, '2018-04-05', 252525),
(15, 2, '2018-04-06', 100),
(16, 3, '2018-04-07', 1665),
(17, 4, '2018-04-08', 1775),
(18, 5, '2018-04-09', 151515);
(14, 1, '2018-04-01', 252525),
(15, 2, '2018-04-02', 100),
(16, 6, '2018-04-03', 1665),
(17, 6, '2018-04-04', 1775),
(18, 6, '2018-04-05', 151515);


/****
                            Test Queries
****/

-- Use this to verify that tables have been created correctly
USE `mydb`;
SELECT * FROM t_users;
SELECT * FROM t_scores;

-- Use this to get all games from one user
USE `mydb`;
SELECT * FROM t_scores WHERE USR_ID = '1';